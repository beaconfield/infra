#!/usr/bin/ruby
# 
# This is a ruby script to interact with the Spacewalk API
#
# Author: Matthew Wilkinson
#
# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/

# ruby gems
require "xmlrpc/client"

# auth info
@SATELLITE_URL = "http://servername.domain.com/rpc/api"
@SATELLITE_LOGIN = "admin"
@SATELLITE_PASSWORD = "password"

# vars
@client = XMLRPC::Client.new2(@SATELLITE_URL)
@key = @client.call('auth.login', @SATELLITE_LOGIN, @SATELLITE_PASSWORD)

# api call
channels = @client.call('channel.listAllChannels', @key)

# for loop to get all the info and print it
for channel in channels do
p channel["label"]
end

# quit
@client.call('auth.logout', @key)
