#!/bin/bash
#
# Author: Matt Wilkinson
#
# Description: Vacuum and backup the PostgreSQL databases
#
# Note: This script assumes you are using Postgres for Jabber as well

# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/

# run this as the postgres user
if [ "$(id -u)" != 26 ]; then
	echo "This script should be run as the postgres user."
	exit 1
fi

### set variables ###
BACKUPDIR="/var/postgres-backups"
DATE=`date +%m-%d-%Y`
DAYS="6"
DB1="spacewalk"
DB2="jabberd2"
ADMIN="user@domain.com"
MAILER="/bin/mail"

## First, run a vacuumdb command to VACUUM and analyze the DB
/usr/bin/vacuumdb --analyze --quiet ${DB1} 
RETVAL="${?}"
if [[ ${RETVAL} -eq 0 ]]; then
	/bin/logger "spacewalk info: postgresql vacuumdb successfully analyzed ${DB1}"
else
	/bin/logger "spacewalk warning: there was a problem running postgresql vacuumdb"
fi

/usr/bin/vacuumdb --analyze --quiet ${DB2} 
RETVAL="${?}"
if [[ ${RETVAL} -eq 0 ]]; then
	/bin/logger "spacewalk info: postgresql vacuumdb successfully analyzed ${DB2}"
else
	/bin/logger "spacewalk warning: there was a problem running postgresql vacuumdb"
fi


# backup the database and compress it
pg_dump -C -f ${BACKUPDIR}/${DB1}_${DATE}.sql ${DB1}
RETVAL="${?}"
if [[ ${RETVAL} -eq 0 ]]; then
        /bin/logger "spacewalk info: postgresql ${DB1} database backup completed successfully."
else
        /bin/logger "spacewalk warning: postgresql database backup failed!"
	echo "check syslog on spacewalk server" | ${MAILER} -s "Spacewalk postgres ${DB1} backup failed" ${ADMIN}
fi

pg_dump -C -f ${BACKUPDIR}/${DB2}_${DATE}.sql ${DB2}
RETVAL="${?}"
if [[ ${RETVAL} -eq 0 ]]; then
        /bin/logger "spacewalk info: postgresql ${DB2} database backup completed successfully."
else
        /bin/logger "spacewalk warning: postgresql database backup failed!"
	echo "check syslog on spacewalk server" | ${MAILER} -s "Spacewalk postgres ${DB2} backup failed" ${ADMIN}
fi


gzip -8 ${BACKUPDIR}/${DB1}_${DATE}.sql
RETVAL="${?}"
if [[ ${RETVAL} -eq 0 ]]; then
        /bin/logger "spacewalk info: postgresql ${DB1} gzip compression task completed successfully."
else
        /bin/logger "spacewalk warning: postgresql gzip compression task failed!"
	echo "check syslog on spacewalk server" | ${MAILER} -s "Spacewalk compression task failed for ${DB1}" ${ADMIN}
fi

gzip -8 ${BACKUPDIR}/${DB2}_${DATE}.sql
RETVAL="${?}"
if [[ ${RETVAL} -eq 0 ]]; then
        /bin/logger "spacewalk info: postgresql ${DB2} gzip compression task completed successfully."
else
        /bin/logger "spacewalk warning: postgresql gzip compression task failed!"
	echo "check syslog on spacewalk server" | ${MAILER} -s "Spacewalk compression task failed for ${DB2}" ${ADMIN}
fi


# remove old database backups
find ${BACKUPDIR} -type f -prune -mtime +${DAYS} -exec rm -f {} \;

exit 0
