#!/bin/bash
#
# Install packages and register an Oracle Linux server with Spacewalk
# Supported Operating Systems: Oracle Linux 5, 6, or 7; CentOS 7
# NOT Supported: RHEL/OL 4.x, Solaris, & HP-UX
# 
# Created by:	Matthew Wilkinson

# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/


### Initialization
### Define the program name and directory, arguments, and version
readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"
readonly VERS="2.9"
readonly RHEL7_EPEL_GPG="gpg-pubkey-352c64e5-52ae6884"
readonly RHEL6_EPEL_GPG="gpg-pubkey-0608b895-4bd22942"
readonly RHEL5_EPEL_GPG="gpg-pubkey-217521f6-45e8a532"
readonly SPACEWALK_COPR="gpg-pubkey-be4b0bc2-5abcfce5"
readonly SPACEWALK_29="gpg-pubkey-9bd837ba-5bfbc74c"
readonly SERVER="spacewalk.domain.com"

# Set this to the latest iteration of the client cert package in either /root/ssl-build
# or /var/www/html/pub/
readonly CLIENT_CERT_PACKAGE="rhn-org-trusted-ssl-cert-1.0-1.noarch.rpm"

### Functions
### Define all of the functions that the script will perform

determine_version() {
	# Determine the OS and set Spacewalk registration key
	grep "release 5." /etc/enterprise-release > /dev/null 2>&1
	if [ ${?} -eq 0 ]; then
		RELEASE="5"
		KEY="1-################################"
	fi

	grep "release 6." /etc/oracle-release > /dev/null 2>&1
	if [ ${?} -eq 0 ]; then
		RELEASE="6"
		KEY="1-################################"
	fi

	grep "release 7." /etc/oracle-release > /dev/null 2>&1
	if [[ ${?} -eq 0 ]]; then
		RELEASE="7"
		KEY="1-################################"
	fi

	# Adding support for CentOS 7
	grep "release 7." /etc/centos-release > /dev/null 2>&1
	if [[ ${?} -eq 0 ]]; then
		RELEASE="C7"
		KEY="1-################################"
	fi
}

check_root() {
	# make sure user is root
	if [ ${EUID} -ne 0 ]; then
		echo "!! Error: This script must be run as root!"
		logger "spacewalk error: this script has to be run as the root user"
		exit 1
	fi
}

check_arch() {
	# make sure 64-bit arch
	uname -i | grep 64 > /dev/null
	if [ ${?} -gt 0 ]; then
		echo "!! Error: This is not a 64-bit machine!"
		logger "spacewalk error: this system does not seem to be 64-bit arch"
		exit 1
	fi
}

check_up2date() {
	# quit the program if up2date is installed
	# up2date conflicts with spacewalk client packages
	# up2date is an archaic RHEL 4 program
	rpm -q up2date > /dev/null 2>&1
	RETVAL=$?
	if [[ ${RETVAL} -eq 0 ]]; then
		echo "!! up2date is installed and will conflict with spacewalk client packages."
		echo "!! Please remove up2date and try again."
		logger "spacewalk error: up2date was found and needs to be removed."
		exit 1
	fi
}

system_id() {
	SYSTEMID="/etc/sysconfig/rhn/systemid"
	if [ -f ${SYSTEMID} ]; then
		echo "Error: Found /etc/sysconfig/rhn/systemid - This system seems to be registered already."
	        echo "You should check Spacewalk to be sure, remove the systemid file, then re-run this script."
	        logger "spacewalk error: system previously registered"
	        exit 1
	fi
}

check_networking() {
	# check to make sure we have access to download files
	# coming soon
	# check for open firewall ports
	# check for http proxy settings...
	/bin/true
}

release5_packages() {
	yum clean all # clean all yum caches
	cd

	echo "===>  Downloading spacewalk client repo..."
	curl -k https://${SERVER}/yum/OracleLinux/OL5/spacewalk5.repo -o /etc/yum.repos.d/spacewalk5.repo

	echo
	# make sure GPG keys for EPEL and Spacewalk exist
	rpm -q ${RHEL5_EPEL_GPG}
	if [[ ${?} -ne 0 ]]; then
		echo "===>  GPG keys do not exist. Importing now..."
		curl -k -O https://${SERVER}/yum/OracleLinux/OL5/spacewalk/RPM-GPG-KEY-EPEL-5
		rpm --import RPM-GPG-KEY-EPEL-5
	fi
    
	# update some packages that need modern versions for spacewalk
	echo "Updating old packages..."
	yum -y update yum rhnlib hal.i386 hal.x86_64

	echo
	echo "===>  Installing required spacewalk client packages..."
	yum -y install rhn-client-tools rhn-check rhn-setup rhnsd m2crypto yum-rhn-plugin osad

	if [ ${?} -gt 0 ]; then
		echo "Error: Installing packages failed!"
		logger "spacewalk error: installing packages failed"
		exit 1
	fi
}

release6_packages() {
	yum clean all # clean all yum caches
	cd
	echo

	echo "===>  Downloading spacewalk client repo..."
	curl -k https://${SERVER}/yum/OracleLinux/OL6/spacewalk.repo -o /etc/yum.repos.d/spacewalk.repo

	echo
	# make gpg keys exist
	rpm -q ${SPACEWALK_29}
	if [[ ${?} -ne 0 ]]; then
		echo "===>  A GPG key does not exist. Importing now..."
		curl -k -O https://${SERVER}/yum/OracleLinux/OL6/spacewalk/copr-spacewalk-pubkey.gpg
		rpm --import copr-spacewalk-pubkey.gpg
	fi
        
	# add Copr GPG key
	rpm -q ${SPACEWALK_COPR}
        if [[ ${?} -ne 0 ]]; then
                echo "===>  A GPG key does not exist. Importing now..."
                curl -k -O https://${SERVER}/yum/OracleLinux/OL7/copr-pubkey.gpg
                rpm --import copr-pubkey.gpg
        fi
 
	echo
	echo "===>  Installing required spacewalk client packages..."
	yum -y install rhn-client-tools rhn-check rhn-setup rhnsd rhnlib m2crypto yum-rhn-plugin osad python2-spacewalk-usix spacewalk-usix

	if [ ${?} -gt 0 ]; then
		echo "Error: Installing packages failed!"
	        logger "spacewalk error: installing packages failed"
		exit 1
	fi
}

release7_packages() {
	yum clean all # clean all yum caches
	cd
	echo
	
	echo "===>  Downloading spacewalk client repo..."
	curl -k https://${SERVER}/yum/OracleLinux/OL7/spacewalk7.repo -o /etc/yum.repos.d/spacewalk7.repo

	echo
	# make sure gpg keys exist
	rpm -q ${SPACEWALK_29}
	if [[ ${?} -ne 0 ]]; then
		echo "===>  A GPG key does not exist. Importing now..."
		curl -k -O https://${SERVER}/yum/OracleLinux/OL7/spacewalk/copr-spacewalk-pubkey.gpg
		rpm --import copr-spacewalk-pubkey.gpg
	fi
    
	# add Copr GPG key
  rpm -q ${SPACEWALK_COPR}
  if [[ ${?} -ne 0 ]]; then
    echo "===>  A GPG key does not exist. Importing now..."
    curl -k -O https://${SERVER}/yum/OracleLinux/OL7/copr-pubkey.gpg
    rpm --import copr-pubkey.gpg
  fi
	
	echo
	echo "===>  Installing required spacewalk client packages..."
	yum -y install rhn-client-tools rhn-check rhn-setup rhnsd rhnlib m2crypto yum-rhn-plugin osad python2-spacewalk-usix spacewalk-usix

	if [ ${?} -gt 0 ]; then
		echo "Error: Installing packages failed!"
	        logger "spacewalk error: installing packages failed"
		exit 1
	fi
}

install_packages() {
	# Install relevant packages
	if [[ ${RELEASE} == "5" ]]; then
		release5_packages
	elif [[ ${RELEASE} == "6" ]]; then
		release6_packages
	elif [[ ${RELEASE} == "7" ]]; then
		release7_packages
	elif [[ ${RELEASE} == "C7" ]]; then
		release7_packages
	else
		echo "Error: This system is not supported by this install script."
		logger "spacewalk error: this system does not seem to be a supported version of Linux"
		exit 1
	fi
}

register_client() {
  echo "****************************************************************************************"
  echo "*                                                                                      *"
  echo "*  Registering this system with the Spacewalk server                                   *"
  echo "*  Performing initial rhn_check to create system profile                               *"
  echo "*  This may take a few moments...                                                      *"
  echo "*                                                                                      *"
  echo "****************************************************************************************"
  echo "Server: https://${SERVER}/"
	rhnreg_ks --serverUrl=https://${SERVER}/XMLRPC --activationkey=${KEY} --force

	if [ $? -eq 0 ]; then
		echo "Registration complete!"
	else
		echo "Registration failed!"
	        logger "spacewalk error: registration failed"
	fi
}

enable_osad() {
	# Determine if osad.conf is already configured.
	# If it is not configured, use sed to configure it properly then start the osad service
	echo "===>  Activating osad..."
	grep "RHN-ORG-TRUSTED-SSL-CERT" /etc/sysconfig/rhn/osad.conf > /dev/null
	if [ ${?} -gt 0 ]; then
		echo "Configuring osad.conf..."
		cp /etc/sysconfig/rhn/osad.conf /etc/sysconfig/rhn/osad.conf.orig
		sed -i 's:osa_ssl_cert =:osa_ssl_cert = /usr/share/rhn/RHN-ORG-TRUSTED-SSL-CERT:g' /etc/sysconfig/rhn/osad.conf
	fi

	service osad restart
	chkconfig osad on
}

### main program function
### Define a main program function that will perform the necessary actions to
### register a client machine with the Spacewalk server
main() {
	echo "===>  Spacewalk install script version ${VERS} starting..."
	echo "===>  Running from: ${PROGDIR}/${PROGNAME}"
	echo

	# Housekeeping
	determine_version
	check_up2date
	check_root
	check_arch

	# check to see if the system is already registered to spacewalk
	# call system_id function to check if client is already registered with spacewalk
	system_id

	# Import SSL certificate
	cd # make sure in the root homedir
	echo "===>  Installing latest Spacewalk Client SSL certificate..."
	curl -O -k https://${SERVER}/pub/${CLIENT_CERT_PACKAGE}
	rpm -Uvh ${CLIENT_CERT_PACKAGE}
	echo

	# Install the required spacewalk client packages
	install_packages

	# Register the client with the Spacewalk server
	register_client
	echo

	# Activate osad service for enabling push actions from the spacewalk server
	enable_osad
	echo

	# disable all other repos since Spacewalk will be the patching server from now on
	echo "===>  Post-install tasks"	
	echo "Disabling all other yum repos..."
	echo "From now on, yum will query the Spacewalk server to see what channels it is subscribed to."
	echo "Utilize Spacewalk's child channels for additional packages."
	echo

	# sed is used to set enabled=0 on all yum .repo files
	sed -i s/enabled=1/enabled=0/g /etc/yum.repos.d/*.repo

	echo "===>  Installation and registration complete!"
	echo "===>  Please manage this host from: https://${SERVER}/"
}

### Execute the main function
main

exit 0
