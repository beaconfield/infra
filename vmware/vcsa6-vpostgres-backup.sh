#!/bin/bash
#
# Author: Matt Wilkinson
#
# Description: wrapper script to execute vPostgres Database backup and restore
# link: https://kb.vmware.com/kb/2091961
#
# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/

# Housekeeping
cd

# set location and name of the file
FILE="/root/postgres_VCDB_backup.bak"

# check for presence of scripts
for f in backup_lin.py restore_lin.py; do
        [ -e "${f}" ] && /bin/true || echo "Scripts not found. Please upload and extract the files from VMware"
        break
done

# backup function
backup () {
        echo "Backing up vPostgres DB..."
        python backup_lin.py -f ${FILE}
}

# restore function
restore () {
        echo "Restoring vPostgres DB..."
        echo "WARNING: the vmware-vpxd and vmware-vdcs services must NOT be running!"
        echo -n "Do you want to continue? y/n: "; read RESP

        case "$RESP" in
                y|Y)
                        echo "OK. Performing the restore..."
                        python restore_lin.py -f ${FILE}
                        ;;
                n|N)
                        echo "OK. Stop the services and try again."
                        ;;
                *)
                        echo "Quitting!"
        esac
}

## main program
if [[ $1 == "backup" ]]; then
        backup
elif [[ $1 == "restore" ]]; then
        restore
else
    echo "Define whether you want to backup or restore the vPostgres DB"
    echo "Usage: $0 {backup|restore}"
    exit 1
fi
