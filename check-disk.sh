#!/bin/bash
#
# Author: Matt Wilkinson
# Note:   Adapted from script found on netsonic.net
#
# Description: Rudimentary script to evaluate disk usage
#              and send an email if any volumes reach or exceed threshold
#
# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/

# Set email address for alerts
EMAIL="user@domain.com"

MAILER="/bin/mail"

# Set max used space threshold in percentage
THRESHOLD="85"

# Set hostname variable
HOSTNAME=$(hostname)

# store all disk info for the email
BODY=""

for LINE in $(df -hP | egrep '^/dev/' | awk '{ print $6 "::" $5 }')
do
        FS=$(echo "${LINE}" | awk -F"::" '{ print $1 }')
        USAGE=$(echo "${LINE}" | awk -F"::" '{ print $2 }' | cut -d'%' -f1 )

        if [ ${USAGE} -ge ${THRESHOLD} -a -z "${BODY}" ];
        then
                BODY="$(date): Running out of diskspace on ${HOSTNAME}\n"
                BODY="${BODY}\n${FS} (${USAGE}%) >= (Threshold = ${THRESHOLD}%)"

        elif [ ${USAGE} -ge ${THRESHOLD} ];
        then
                BODY="${BODY}\n${FS} (${USAGE}%) >= (Threshold = ${THRESHOLD}%)"
        fi
done 

if [ -n "${BODY}" ];
then
        echo -e "${BODY}" | ${MAILER} -s "Alert: Partition(s) almost out of diskspace on ${HOSTNAME}" ${EMAIL}
fi
