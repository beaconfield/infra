#!/bin/bash
#
#
# Description: Deploy a new version of Tomcat and copy config files from current version
#              Includes dealing with an existing SSL keystore file and war file.
#              Assumes some basic configuration of Tomcat is already done as well as an init script
#
# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/

## Adding for gitlab

echo "====>  Tomcat upgrade taking place. Preforming pre-flight checks..."

# make sure user is root
if [ ${EUID} -ne 0 ]; then
	echo "====>  Error: This script must be run as root!"
	exit 1
fi

# check number of arguments provided
if [[ ${#} != "3" ]]; then
    echo "====>  Wrong number of parameters provided"
	echo ""
    echo "Usage:"
    echo "${0} <Package_URL> <New_Tomcat_Version> <Current_Tomcat_Version>"
	echo ""
	echo "Example:
${0} http://<webserver>/tomcat/apache-tomcat-7.0.67.tar.gz 7.0.67 7.0.65"
    exit 1
fi

PACKAGE_LOCATION=${1} # The web server location where the Tomcat tarball resides
TOMCAT_VERSION=${2} # The new numerical version of Tomcat that we're upgrading to
CURRENT_VERSION=${3} # The currently running numerical version of Tomcat

# Setup the necessary variables
DIR="/opt"
CATALINA_HOME="/opt/apache-tomcat-${TOMCAT_VERSION}"
CONF_HOME="${CATALINA_HOME}/conf"
WEBAPP_HOME="${CATALINA_HOME}/webapps"
TOMCAT_USER="user"
TOMCAT_GROUP="group"
WARFILE="filename.war"
KEYSTORE="filename.keystore"

# check if Tomcat is currently running
ps -ef | grep catalina | grep -v grep > /dev/null 2>&1
RETVAL=${?}
if [[ ${RETVAL} -eq "0" ]]; then
	echo "====>  Error: Tomcat seems to be running. Please stop Tomcat before the upgrade."
	echo "====>  Quitting."
	exit 1
fi

# Check if the build is the same
if (( $(echo "${CURRENT_VERSION} > ${TOMCAT_VERSION})" | bc -l )); then
	echo "====>  Error: The TOMCAT_VERSION must be newer than the CURRENT_VERSION."
	exit 1
fi

# Make sure the current version actually exists
if [[ ! -d /opt/apache-tomcat-${CURRENT_VERSION} ]]; then
	echo "====>  Error: The current Tomcat directory does not seem to exist..."
	echo "====>  Please check /opt and the version number."
	exit 1
fi

# Make sure the current version is configured as the default version
grep ${CURRENT_VERSION} /home/${TOMCAT_USER}/.bash_profile > /dev/null 2>&1
RETVAL=${?}
if [[ ${RETVAL} != "0" ]]; then
	echo "====>  Error: The Tomcat version ${CURRENT_VERSION} does not seem to be configured as the default version."
	echo "====>  This may happen when an older version of Tomcat was entered as the current version."
	echo "====>  Please check the Tomcat variables in /etc/init.d/tomcat7 and the user's .bash_profile and try again."
	exit 1
fi

echo "====>  Properties set. Deploying new Tomcat..."

cd ${DIR} > /dev/null

# Download the tarball and make necessary changes
echo "====>  Downloading and extracting package ${PACKAGE_LOCATION}..."
curl -f -s -O ${PACKAGE_LOCATION}
STATUS=${?}
if [[ ${STATUS} != "0" ]]; then
    echo "====>  Error: The download of the package failed! Package location was: ${PACKAGE_LOCATION}"
    exit 1
fi
echo "====>  Download completed"

# Extract and change ownership
if [[ ! -d apache-tomcat-${TOMCAT_VERSION} ]]; then
	tar xzf apache-tomcat-${TOMCAT_VERSION}.tar.gz
	chown -R ${TOMCAT_USER}:${TOMCAT_GROUP} apache-tomcat-${TOMCAT_VERSION}
	rm -f apache-tomcat-${TOMCAT_VERSION}.tar.gz # clean up tarball
	echo "====>  Tarball extracted. Permissions set."
else
	echo "====>  Error: New Tomcat directory already exists!"
	echo "====>  Cleaning up"
	rm -f apache-tomcat-${TOMCAT_VERSION}.tar.gz
	exit 1
fi

# backup environment and init script before updating Tomcat version
echo ""
cp -vp /home/${TOMCAT_USER}/.bash_profile /home/${TOMCAT_USER}/.bash_profile.tomcatbakup
sed -i s/apache-tomcat-${CURRENT_VERSION}/apache-tomcat-${TOMCAT_VERSION}/g /home/${TOMCAT_USER}/.bash_profile
cp -v /etc/init.d/tomcat7 /root/tomcat7.init.backup
sed -i s/apache-tomcat-${CURRENT_VERSION}/apache-tomcat-${TOMCAT_VERSION}/g /etc/init.d/tomcat7
echo ""
echo "====>  Environment and init script updated."

echo "====>  Copying the necessary configuration files to the new instance..."
# Copy server.xml file and adjust Tomcat version
echo ""
cp -vp /opt/apache-tomcat-${CURRENT_VERSION}/conf/server.xml ${CONF_HOME}
sed -i s/${CURRENT_VERSION}/${TOMCAT_VERSION}/g ${CONF_HOME}/server.xml

# Copy keystore for Tomcat SSL
cp -vp /opt/apache-tomcat-${CURRENT_VERSION}/conf/${KEYSTORE} ${CONF_HOME}

# Copy the war file into new Tomcat webapps directory
cp -vp /opt/apache-tomcat-${CURRENT_VERSION}/webapps/${WARFILE} ${WEBAPP_HOME}

echo ""
echo "====>  Config files copied. Apache Tomcat version ${TOMCAT_VERSION} successfully deployed."
echo "====>  You may start Tomcat7 via:"
echo "====>  /etc/init.d/tomcat7 start"
echo "====>  Notice: Make sure tomcat is now running on the upgraded version."
echo ""

exit 0
