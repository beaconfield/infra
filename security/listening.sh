#!/bin/bash
#
# Original listeningports script by...: Ryan Fortman
# Modified by.........................: Matt Wilkinson
#
# GPL License:
#
# This program is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see http://www.gnu.org/licenses/

echo
echo "Hostname: $(hostname)"

# evaluates all LISTENING ports on the system
if [[ ${USER} != "root" ]]; then
   echo
   echo
   echo "Only listing ports that user \"${USER}\" has access to, run as root if all ports are needed"
   echo
else
   echo
   echo
fi

OPENPORTS=$( netstat -ntl | egrep ^tcp | awk ' { print $4 } ' | awk -F":" ' { print $NF } ' | sort -n | uniq )

for PORT in ${OPENPORTS}; do
	LSOF=$(lsof -i :${PORT})

	if [[ "${LSOF}" != "" ]]; then
		echo "---------------------------------------------------"
		echo "LISTEN PORT: ${PORT}"
		LSOF_PIDS=$( echo "${LSOF}" | awk ' { print $2 } ' | grep -v "PID" | sort -n | uniq | tr "\n" " " )
		echo "${LSOF}" | awk ' { print "\t"$0 } '
		echo
		echo "Process information:" | awk ' { print "\t"$0 } '

		for LSOF_PID in ${LSOF_PIDS}; do
			ps -ef | awk -v LSOF_PID="${LSOF_PID}" ' $2==LSOF_PID ' | awk ' { print "\t"$0 } '
		done
		echo
	fi

done

exit 0
